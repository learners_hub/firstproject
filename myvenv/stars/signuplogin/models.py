from django.db import models
from django.contrib.auth.models import User

class UserLoginLogoutEvent(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    login_user = models.CharField(max_length=10, null=True)  # 'login' or 'logout'
    logout_user = models.DateTimeField(null=True)

    def __str__(self):
        return self.user, self.login_user,self.logout_user

